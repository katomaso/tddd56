/*
 * Placeholder for wavelet transform.
 * Currently just a simple invert.
 */

// assume local size being 512

#define is_top(pos) (pos > 512 * 509)
#define is_right(pos)  (pos % 512 > 509)

#define X_HALF (256*3)
#define Y_HALF (512*3*255)

__kernel void kernelmain(__global unsigned char *image, __global unsigned char *data, const unsigned int length)
{
  unsigned int i = get_global_id(0), pos = i/3;  
  unsigned int x, y, quatre;
  int in1, in2, in3, in4;
  int out1, out2, out3, out4;
  
  x = i % 1536;
  y = i / 1536;
  
	if( is_top(pos) || is_right(pos)) 
	{
		in1 = in2 = in3 = in4 = image[i];
	} 
	else 
	{
	  if( (pos % 2 == 1)      ) return; // skip each odd column
    if( (pos / 512) % 2 == 1) return; // skip each odd row
  
		in1 = image[i];
		in2 = image[i+3];
		in3 = image[i+512*3];
		in4 = image[i+513*3];
	}
  quatre = (y/2)*1536 + ((pos%512)/2)*3 + x%3;
  
  out1 = (in1 + in2 + in3 + in4) >> 2;
	out2 = (((in1 + in2 - in3 - in4) >> 2)+255) >> 1;
	out3 = (((in1 - in2 + in3 - in4) >> 2)+255) >> 1;
	out4 = (((in1 - in2 - in3 + in4) >> 2)+255) >> 1;
	
	data[quatre] = out1;
  data[quatre + X_HALF] = out2;
	data[quatre + Y_HALF] = out3;
	data[quatre + X_HALF + Y_HALF] = out4;
}
