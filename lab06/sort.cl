/*
 * Rank sorting in sorting OpenCL
 * This kernel has a bug. What?
 */

__kernel void sort(__global unsigned int *idata, 
                   __global unsigned int *odata,
                   const unsigned int length)
{ 
  unsigned int pos = 0;
  unsigned int i, b;
  unsigned int val;
  __local unsigned int buffer[512];
  
  val = idata[get_global_id(0)];
  
  for(b = 0; b < length/512; b++) {
    buffer[get_local_id(0)] = idata[get_local_id(0) + 512*b];
    barrier(CLK_LOCAL_MEM_FENCE);
    
    //find out how many values are smaller
     for (i = 0; i < 512; i++)
      if (val > buffer[i])
        pos++;

    barrier(CLK_LOCAL_MEM_FENCE);
  }
  
  odata[pos]=val;
}



