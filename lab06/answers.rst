:Title: Laboration 6: Introduction to OpenCL
:Author: Tomas Peterka & Thomas Uhrig

Laboration 6: Introduction to OpenCL
====================================

1. Getting started: Hello, World!
---------------------------------

**Question:** *How is the communication between the host and the graphic card handled?*
  Data exchange is done via ``clCreateBuffer`` and ``clReadBuffer``. When 
  creating buffer we have to set a flag if the memory is in/out/both.

  The other communication is code uploading. This is done via functions
  ``clCreateKernel``, ``clSetKernelArg`` and finaly ``clEnqueueNDRangeKernel``.
  When creating kernel the first argument has to be a binary with compiled
  kernel code. This is reason why the kernel code has to be in string or in a
  separate file.

**Question:** *What function executes your kernel?*
  ``clEnqueueNDRangeKernel``

  It is wort to mention that openCL has more ``clEnqueue*`` methods.

**Question:** *How does the kernel know what element to work on?*
  In kernel function are predefined *position functions* which are
   * ``get_global_id(int n)`` - gets ID of a *work-item* from global 
     point of view
   * ``get_local_id(int n)`` - gets ID of work-item relative to work-group
   * ``get_group_id(int n)`` - gets ID of current work-group

  All these functions takes one parameter - ``int n`` which specifies which part
  (dimension) of location will be returned if the location is specified as
  multi-dimensional.


2. Rank sorting on the GPU
--------------------------

2.1 Bad approach, do it better
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Question:** *What was the problem, and the fix?*
  Problem is when a thread finishes earlier. We have splited data to input
  and output data
  ::
  
  __kernel void sort(__global unsigned int *idata, 
                   __global unsigned int *odata,
                   const unsigned int length)
  { 
    unsigned int pos = 0;
    unsigned int i;
    unsigned int val;

    //find out how many values are smaller
    for (i = 0; i < get_global_size(0); i++)
      if (idata[get_global_id(0)] > idata[i])
        pos++;

    val = idata[get_global_id(0)];
    
    odata[pos]=val;
  }
  


2.2 Improve performance with local memory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Question:** *How did you take advantage of local memory?*
  We split the problem to blocks 512 ints long. The number of blocks is according
  to realnumber of blocks in graphic card. When we load whole block into local
  memory then all threads which has to iterate over all 512 elements make it much
  more faster (2times faster).
  
**Question:** *What speedup did you get, and why?*
  The speedup is around **2 times**.
  
  **Old performance**
  +------+----------+----------+
  | CPU  | 85684 us |	84148 us |
  +------+----------+----------+
  | GPU  |  1083 us	|  1073 us |
  +------+----------+----------+

  **Optimized performance**
  +------+----------+----------+
  | CPU  | 87154 us |	84454 us |
  +------+----------+----------+
  | GPU  |  472 us	|  468 us  |
  +------+----------+----------+
  
::

  __kernel void sort(__global unsigned int *idata, 
                   __global unsigned int *odata,
                   const unsigned int length)
  { 
    unsigned int pos = 0;
    unsigned int i, b;
    unsigned int val;
    __local unsigned int buffer[512];
    
    val = idata[get_global_id(0)];
    
    for(b = 0; b < length/512; b++) {
      buffer[get_local_id(0)] = idata[get_local_id(0) + 512*b];
      barrier(CLK_LOCAL_MEM_FENCE);
      
      //find out how many values are smaller
       for (i = 0; i < 512; i++)
        if (val > buffer[i])
          pos++;

      barrier(CLK_LOCAL_MEM_FENCE);
    }
    
    odata[pos]=val;
  }


3. Wavelet transform
--------------------

**Question:** *What speedup did you get compared to your CPU version?*
  Approximate speedup is between **7 and 8 times**.
	+------+----------+----------+
  | CPU  |  8369 us |	8578 us  |
  +------+----------+----------+
  | GPU  |  1092 us	| 1411 us  |
  +------+----------+----------+


**Question:** *Why do you think a wavelet coded image can be easier to compress than the original?*
  Because it contains lots of similar values (or precisely the same) thus e.g. bz2
  will have very high compression rate.


4. evaluation
-------------

**Evaluation question:** *Part 3 is new this year, in order to expand the 
OpenCL lab a little bit. (It was clearly too small before.) However, we wanted 
to expand carefully, to stay on the managable side. Was it interesting enough,
worthwhile, or still too easy? Would it have been better to get less ready-made
code and adapt part 2 instead?*
  I appreciated every time new algorithm -- fractal, sorting and image filter
  are the best! **BUT** I would prefer bigger help with the algorithm, otherwise
  one get stucked on stupid indexes, negatives values and border IFs. I have 
  spent on this more than two hours and it has mostly nothing in common with 
  openCL, it is just annoying. I would recommend less prepared openCL code 
  (device initization, kernel enqueing ...) and more prepared algorithms code.

**Evaluation question:** *Do you think we have a reasonable mix of CUDA and OpenCL?*
  I feel fully content with the mix of CUDA and openCL. IMHO very well balanced.
  Thank you.
  
