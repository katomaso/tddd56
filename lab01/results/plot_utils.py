#coding: utf-8
import io
import numpy as np


def load_header_and_data(filename = "data.m"):
    header = list()
    data = None

    with io.BufferedReader(io.FileIO(filename, "r")) as datafile:
	# extract header
	for line in datafile:
	    if line.strip().startswith("%"):
		header = line.strip().lstrip("%").split()
		break
	# load data
	dd = list()
	for line in datafile:
	    if line.startswith("]"):
		break
	    dd.append(map(float, line.rstrip().split()))
	data = np.array(dd, dtype=np.float32)
    return header, data

