#!/usr/bin/env python2
#coding: utf-8

from __future__ import absolute_import

import sys
import numpy as np
import matplotlib.pyplot

import plot_utils

filename = "data.m"
if len(sys.argv) > 1:
    filename = sys.argv[1]

header, data = plot_utils.load_header_and_data(filename)
