#!/usr/bin/env python2
"""
Usage:
  ./frac.py [options]

Options:
  -w W --width=W            Width of image [default: 800]
  -h H --height=H           Height of image [default: 600]
  --real_min=<rmin>         Minimum of real part [default: -2.0]
  --real_max=<rmax>         Maximum of real part [default: 0.6]
  --imag_min=<imin>         Minimum of imaginary part [default: -1]
  --imag_max=<imax>         Maximum of imaginary part [default: 1]
  -i I --infinity=I         Which value is counted as infinity [default: 2]
  -m M --max_iter=M         Iterations per pixel (0, 255) [default: 255]
  -o FILE --output=FILE     Image output file. Please use suffixes jpg or png [default: mandelbrot.png]
  --debug
  --quiet
"""

import array
from docopt import docopt
from PIL import Image

def compute_color(c, maxiters, infinity):
  i = 0
  z = complex()
  while abs(z) < infinity and i < maxiters:
    z = z*z + c
    i += 1
  return i


def pretify(color):
  if color == 0xff: return 0xB2D7EF
  r = (~color & 0b11111111) >> 1
  g = (~color & 0b11111111) << 7 | (~color & 0b00000010) <<  14
  b = (~color & 0b11111111) << 16
  return r|g|b


options = docopt(__doc__, version=1.0)

DEBUG = options['--debug']
quiet = options['--quiet']

width = int(options["--width"])
height = int(options["--height"])
maxiter = int(options["--max_iter"])
infinity = float(options["--infinity"])

assert 0 < maxiter < 256, "max_iter is not in allowed interval"

basepoint = c = complex(float(options["--real_min"]), float(options["--imag_max"]))  # starting upper left corner
stepx = complex((float(options["--real_max"]) - float(options["--real_min"]))/width, 0.0)
stepy = complex(0.0, (float(options["--imag_max"]) - float(options["--imag_min"]))/height)

imagedata = array.array('i')

if DEBUG:
  print("Running with arguments \nW {0}, \nH {1}, \nstepX {2}, \nstepY {3}, \n infinity {4}"\
      .format(width, height, stepx, stepy, infinity))
  print("")

hn = range(height)
wn = range(width)

for h in hn:
  for w in wn:
    c += stepx
    imagedata.append(pretify(compute_color(c, maxiter, infinity)))
  if not quiet: print("%d %% done" % int(h/height*100))
  c = basepoint - (stepy * (h+1))


#~ if DEBUG:
  #~ for h in range(height):
    #~ for w in range(width):
      #~ print "." if imagedata[w+w*h] > 128 else " ",
    #~ print("")

if not quiet: print("Generating image")
image = Image.new(mode="RGB", size=[width, height])
image.putdata(imagedata)
image.save(options["--output"])
