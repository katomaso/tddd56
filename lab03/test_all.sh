for OWN in `seq 1 10`
do
	make NB_THREADS=2 VARIANT=1 MEASURE=2 SUFFIX="-own$OWN" OWN=${OWN}
done

for OWN in `seq 1 10`
do
	./sort-own$OWN rand-100000
	sleep 1
	echo ""
	echo "----------------------------------------"
	echo ""
done

