/*
 * sort.c
 *
 *  Created on: 5 Sep 2011
 *  Copyright 2011 Nicolas Melot
 *
 * This file is part of TDDD56.
 *
 *     TDDD56 is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     TDDD56 is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with TDDD56. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// Do not touch or move these lines
#include <stdio.h>
#include "disable.h"
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#ifndef DEBUG
#define NDEBUG
#endif

#define MAX_PRINT_LENGTH 101

#include <stdbool.h>
#include "array.h"
#include "sort.h"
#include "simple_quicksort.h"

#define MAX(x,y) x < y ? y : x
#define MIN(x,y) x < y ? x : y
#define MEDIAN(x, y, z) (x <= y ? (z < x ? x : \
        (z > y ? y : z)) : (z < y ? y : (z > x ? x : z)))

#define SWAP(a,b) {value tmp = global_array->data[a]; \
global_array->data[a] = global_array->data[b]; \
global_array->data[b] = tmp;}

    //~ OWN values
        //~ 1 = full parallel quick sort
        //~ 2 = parallel bitonic sort
        //~ 3 = sequentiell bitonic sort
        //~ 4 = parallel quick sort
void* quick_sort_iterative(void*);
        //~ 5 = sequentiell quick sort
        //~ 6 = sequentiell selection sort
        //~ 7 = sequentiell insertion sort
        //~ 8 = parallel quick sort with recursion break (insertion sort)
        //~ 9 = parallel bitonic quick sort with recursion break (insertion sort)
//~
        //~ 10 = iterative quick sort
//~
        //~ else = given sequentiell quick sort

#ifndef OWN
#define OWN 2
#endif

/*
 * Threshold on the length of array when stop paralleling and just
 * sort it sequentially. For testing purposes low number. In real world
 * about 10000.
 */
#if DEBUG == 1
#define MIN_PAR_LENGTH 3
#else
#define MIN_PAR_LENGTH 3000
#endif

/*
 * global_array is just pointer to source array
 * outplace_array is for outplace algorithms to use this array
 */
struct array *global_array;
value *outplace_array;

/*
 * synchronization structures
 * level is ....?!
 */
pthread_attr_t attr;
pthread_t threads[NB_THREADS*2];
pthread_mutex_t level_mutex;
int level = 0;

/*
 * Unified parameters for sorting algorithms. `result` is intended as IO
 * variable. `pivot` can be used with non-pivot sorting algs to store
 * value with different meaning.
 */
struct sort_args {

        /* start, end are used as <start, end) */
        int start, end, dir, pivot, result;

        /* shared variables between threads at one level of recursion
         * special variables for parallel_qs which point to the value which stores
         * where current index in outplace_array is.
         */
        int *left, *right;
};
typedef struct sort_args sort_args_t;


inline void set(int index, int value) {

        array_insert(global_array, value, index);
}

inline void swap(int a, int b)
{
        int value_a = global_array->data[a];
        int value_b = global_array->data[b];

        array_insert(global_array, value_a, b);
        array_insert(global_array, value_b, a);
}

inline void compare(int i, int j, bool dir)
{
        #if DEBUG == 1
        printf("compare i %d and j %d\n", i, j);
        printf("compare global[i] = %d\n", global_array->data[i]);
        printf("compare global[j] = %d\n", global_array->data[j]);
        #endif

        if (dir == (global_array->data[i] > global_array->data[j]))
                swap(i, j);
}

void find_pivot(sort_args_t* args) {
        if ( args->start >= (args->end - 1)) {
                args->pivot = global_array->data[args->start];
                return;
        }
        value x,y,z;
        x = global_array->data[args->start];
        y = global_array->data[(args->start + args->end) / 2];
        z = global_array->data[args->end-1];
        #if DEBUG == 1
        printf("deciding pivot between %d, %d, %d\n", x,y,z);
        #endif
        args->pivot = MEDIAN(x,y,z);
        //~ if(median == x) args->pivot = args->start;
        //~ if(median == y) args->pivot = (args->start + args->end) / 2;
        //~ if(median == z) args->pivot = args->end-1;
}

void find_pivot_index(sort_args_t* args) {
        if ( args->start >= (args->end - 1)) {
                args->pivot = global_array->data[args->start];
                return;
        }
        value x,y,z, median;
        x = global_array->data[args->start];
        y = global_array->data[(args->start + args->end) / 2];
        z = global_array->data[args->end-1];
        #if DEBUG == 1
        printf("deciding pivot between %d, %d, %d\n", x,y,z);
        #endif
        median = MEDIAN(x,y,z);
        if(median == x) args->pivot = args->start;
        if(median == y) args->pivot = (args->start + args->end) / 2;
        if(median == z) args->pivot = args->end-1;
}

inline void selection_sort(int from, int to)
{
    int i, k, t, min;

    for(i = from; i < to-1; i++)
    {
        min = i;
        for( k = i+1; k < to; k++)
        {
            if(global_array->data[k] < global_array->data[min])
                min = k;
        }

        t = global_array->data[min];
        set(min, global_array->data[i]);
        set(i, t);
    }
 }

 inline void insertion_sort(int from, int to)
 {
        int tmp;

        int i, j;
        for(i = from + 1; i < to; i++)
        {
                tmp = global_array->data[i];

                j = i - 1;

                while(tmp < global_array->data[j] && j >= 0)
                {
                        set(j + 1 , global_array->data[j]);
                        j = j - 1;
                }

                set(j + 1, tmp);
        }
}

inline int greatestPowerOfTwoLessThan(int n)
{
        int k=1;

        while (k<n)
                k = k * 2;

        return k / 2;
}

inline void bitonic_merge(int from, int n, bool dir)
{
        #if DEBUG == 1
        printf("in bitonic merge from %d for %d elements\n", from, n);
        #endif

        if (n > 1)
        {
                int m = greatestPowerOfTwoLessThan(n);

                int i;

                for (i = from; i < (from + n - m); i++)
                        compare(i, i + m, dir);

                bitonic_merge(from, m, dir);
                bitonic_merge(from + m, n-m, dir);
        }
}

void bitonic_sort(int from, int n, bool dir)
{
        #if DEBUG == 1
        printf("in bitonic sort from %d for %d elements\n", from, n);
        #endif

        if (n > 1)
        {
                int m = n/2;

                bitonic_sort(from, m, !dir);
                bitonic_sort(from + m, n-m, dir);

                bitonic_merge(from, n, dir);
        }
}

void* bitonic_sort_parallel(void* args)
{
        sort_args_t *b = (sort_args_t*) args;
        #if DEBUG == 1
        printf("in bitonic sort parallel from %d for %d elements\n", b->start, b->end);
        #endif

        if(level < NB_THREADS) {

                if (b->end > 1)
                {
                        #if DEBUG == 1
                        printf("spawn 2 new bitonic thread\n");
                        #endif

                        int m = b->end/2;

                        // spawn it in a thread
                        sort_args_t b1 = { b->start, m, !b->dir };
                        sort_args_t b2 = { b->start + m, b->end - m, b->dir };

                        pthread_mutex_lock(&level_mutex);

                        level = level + 2;

                        pthread_t t1 = threads[level - 1];
                        pthread_t t2 = threads[level - 2];

                        pthread_mutex_unlock(&level_mutex);

                        pthread_create(&t1, &attr, bitonic_sort_parallel, (void*)&b1);
                        pthread_create(&t2, &attr, bitonic_sort_parallel, (void*)&b2);

                        pthread_join(t1, NULL);
                        pthread_join(t2, NULL);

                        bitonic_merge(b->start, b->end, b->dir);
                }
        }
        else{

                bitonic_sort(b->start, b->end, b->dir);
        }
        return NULL;
}

void quick_sort(int beg, int end, bool dir)
{
        if (end > beg + 1)
        {
                int piv = global_array->data[beg];
                int l = beg + 1;
                int r = end;

                while (l < r)
                {
                        if (dir == (global_array->data[l] <= piv))
                                l++;
                        else
                                swap(l, --r);
                }

                swap(--l, beg);

                quick_sort(beg, l, dir);
                quick_sort(r, end, dir);
        }
}

/*
// sensitive variant of QS but still buggy
void* quick_sort_parallel(void* args)
{
        sort_args_t *b = (sort_args_t*) args;

        if (b->end > b->start + 1)
        {
                find_pivot_index(args);
                int l = b->start;
                int r = b->end;
                int swaps = 0;
                #if DEBUG == 1
                printf("parallel partition <%d, %d); pivot %d\n", b->start, b->end, global_array->data[b->pivot]);
                #endif
                while (l < r)
                {
                        while ((b->dir == (global_array->data[l] <= global_array->data[b->pivot])) && l < b->pivot) l++;
                        while ((b->dir == (global_array->data[r] > global_array->data[b->pivot])) && r > b->pivot) r--;

                        if (l < r)
                        {
                                SWAP(l, r);
                        }
                }
                if(l >= b->end) return NULL;
                #if DEBUG == 1
                printf("After partitioning left: %d, right: %d\n", l, r);
                if(global_array->length < MAX_PRINT_LENGTH) array_printf(global_array);
                #endif
                sort_args_t b1 = { b->start, l, b->dir };
                sort_args_t b2 = { r, b->end, b->dir };

                if(level < NB_THREADS)
                {
                        pthread_mutex_lock(&level_mutex);

                        level = level + 2;

                        pthread_t t1 = threads[level - 1];
                        pthread_t t2 = threads[level - 2];

                        pthread_mutex_unlock(&level_mutex);

                        pthread_create(&t1, &attr, quick_sort_parallel, (void*) &b1);
                        pthread_create(&t2, &attr, quick_sort_parallel, (void*) &b2);

                        pthread_join(t1, NULL);
                        pthread_join(t2, NULL);
                        return NULL;

                }
                quick_sort_parallel((void*) &b1);
                quick_sort_parallel((void*) &b2);
                return NULL;

        }
        return NULL;
}
*/

void* quick_sort_parallel(void* args)
{
        sort_args_t *b = (sort_args_t*) args;

        if(level < NB_THREADS)
        {
                if (b->end > b->start + 1)
                {
                        int piv = global_array->data[b->start];
                        int l = b->start + 1;
                        int r = b->end;

                        while (l < r)
                        {
                                if (b->dir == (global_array->data[l] <= piv))
                                        l++;
                                else
                                        swap(l, --r);
                        }

                        swap(--l, b->start);

                        pthread_mutex_lock(&level_mutex);

                        level = level + 2;

                        pthread_t t1 = threads[level - 1];
                        pthread_t t2 = threads[level - 2];

                        pthread_mutex_unlock(&level_mutex);

                        sort_args_t b1 = { b->start, l, b->dir };
                        sort_args_t b2 = { r, b->end, b->dir };

                        pthread_create(&t1, &attr, quick_sort_parallel, (void*) &b1);
                        pthread_create(&t2, &attr, quick_sort_parallel, (void*) &b2);

                        pthread_join(t1, NULL);
                        pthread_join(t2, NULL);
                }
        }
        else
        {
                quick_sort(b->start, b->end, b->dir);
        }
        return NULL;
}


void* quick_sort_parallel_insertion(void* args)
{
        sort_args_t *b = (sort_args_t*) args;
        // break recursion by a list shorter then 10
        if((b->end - b->start) < MIN_PAR_LENGTH)
        {
                insertion_sort(b->start, b->end);
        }
        else
        {
                return quick_sort_parallel(b);
        }
        return NULL;
}

void* bitonic_quick_sort_parallel(void* args)
{
        sort_args_t *b = (sort_args_t*) args;
        #if DEBUG == 1
        printf("in bitonic quick sort parallel from %d for %d elements\n", b->start, b->end);
        #endif

        if(level < NB_THREADS) {

                if (b->end > 1)
                {
                        #if DEBUG == 1
                        printf("spawn 2 new bitonic thread\n");
                        #endif

                        int m = b->end/2;

                        // spawn it in a thread
                        sort_args_t b1 = { b->start, m, !b->dir };
                        sort_args_t b2 = { b->start + m, b->end - m, b->dir };

                        pthread_mutex_lock(&level_mutex);

                        level = level + 2;

                        pthread_t t1 = threads[level - 1];
                        pthread_t t2 = threads[level - 2];

                        pthread_mutex_unlock(&level_mutex);

                        pthread_create(&t1, &attr, bitonic_sort_parallel, (void*) &b1);
                        pthread_create(&t2, &attr, bitonic_sort_parallel, (void*) &b2);

                        pthread_join(t1, NULL);
                        pthread_join(t2, NULL);

                        bitonic_merge(b->start, b->end, b->dir);
                }
        }
        else{

                if (b->end > 1)
                {
                        #if DEBUG == 1
                        printf("spawn 2 new bitonic thread\n");
                        #endif

                        int m = b->end/2;

                        // spawn it in a thread
                        sort_args_t b1 = { b->start, m, !b->dir };
                        sort_args_t b2 = { b->start + m, b->end - m, b->dir };

                        quick_sort_parallel_insertion((void*) &b1);
                        quick_sort_parallel_insertion((void*) &b2);

                        bitonic_merge(b->start, b->end, b->dir);
                }
        }
        return NULL;
}


/*
 * Swap elements according to `pivot`
 */
void* partition(void* args) {

        sort_args_t *b = (sort_args_t*) args;
        #if DEBUG == 1
        printf("partition form %d to %d with pivot %d\n", b->start, b->end, b->pivot);
        #endif

        if(b->start >= b->end) return NULL;

        //forall i in 0...n-1 do in parallel
        int i;
        value v;
        for(i = b->start; i < b->end; i++) {
                v = global_array->data[i];

                if (b->dir == (v < b->pivot)) {
                        outplace_array[__sync_fetch_and_add(b->left, 1)] = v;
                }
                else {
                        outplace_array[__sync_fetch_and_add(b->right, -1)] = v;
                }
        }
        return NULL;
}

/*
 * Copy from outplace to global
 */
void* copytion(void* args) {

        sort_args_t *b = (sort_args_t*) args;
        #if DEBUG == 1
        printf("copy form %d to %d with pivot %d\n", b->start, b->end, b->pivot);
        #endif

        if(b->start >= b->end) return NULL;

        memcpy(
                (void*)(global_array->data+b->start),   // dest
                (void*)(outplace_array+b->start),               // src
                sizeof(value) * (b->end - b->start)             // size
        );

        return NULL;
}

/*
 * Swap elements according to `pivot` in parallel
 */
void* parallel_partition(void* args)
{
        sort_args_t *b = (sort_args_t*) args;
        int size = b->end - b->start;
        if( size > MIN_PAR_LENGTH*NB_THREADS ) {

                sort_args_t par_args[NB_THREADS];
                int chunk = (size / NB_THREADS), i;

                #if DEBUG == 1
                printf("parallel partition <%d, %d); pivot %d; total size %d\n", b->start, b->end, b->pivot, size);
                #endif

                for(i=0; i < NB_THREADS; i++) {
                        par_args[i] = *b;
                        par_args[i].start = b->start + i * chunk;
                        par_args[i].end   = b->start + (i+1) * chunk;

                        if( i == NB_THREADS-1 ) par_args[i].end = b->end;

                        pthread_create(&threads[i+b->result*NB_THREADS], &attr, partition, (void*) (par_args+i));
                }
                for(i=0; i < NB_THREADS; i++) {
                        pthread_join(threads[i+b->result*NB_THREADS], NULL);
                }

                for(i=0; i < NB_THREADS; i++) {
                        par_args[i] = *b;
                        par_args[i].start = b->start + i * chunk;
                        par_args[i].end   = b->start + (i+1) * chunk;

                        if( i == NB_THREADS-1 ) par_args[i].end = b->end;

                        pthread_create(&threads[i+b->result*NB_THREADS], &attr, copytion, (void*) (par_args+i));
                }
                for(i=0; i < NB_THREADS; i++) {
                        pthread_join(threads[i+b->result*NB_THREADS], NULL);
                }
        }
        else
        {
                #if DEBUG == 1
                printf("sequential partition <%d, %d); pivot %d; l %d, r %d\n", b->start, b->end, b->pivot, *b->left, *b->right);
                #endif
                partition(args);
                copytion(args);
        }

        #if DEBUG == 1
        printf("After partitioning left: %d, right: %d\n", *b->left, *b->right);
        #endif

        // don't sort the pivots again
        while(outplace_array[*b->left] == b->pivot) (*b->left)++;
        while(outplace_array[*b->right] == b->pivot) (*b->right)--;

        #if DEBUG == 1
        printf("After skipping pivots left: %d, right: %d\n", *b->left, *b->right);
        #endif
        #if DEBUG == 1
        if(global_array->length < MAX_PRINT_LENGTH) array_printf(global_array);
        #endif

        return NULL;
}


/*
 * quick sort goes in an array <start, end)
 */
void* quick_sort_full_parallel(void* args)
{
        sort_args_t *b = (sort_args_t*) args;

        int left = b->start, right = b->end-1;
        int size = b->end - b->start;

        b->left = &left;
        b->right = &right;


        if (size > MIN_PAR_LENGTH*NB_THREADS )
        {
                // parallel partitioning
                int l1, r1, l2, r2;
                find_pivot(args);

                parallel_partition(args);

                l1 = b->start, r1 = *b->right;
                l2 = *b->left, r2 = b->end-1;

                sort_args_t args1 = { b->start, *b->right+1, b->dir, 0, b->result, &l1, &r1 };
                sort_args_t args2 = { *b->left, b->end, b->dir, 0, b->result, &l2, &r2 };

                quick_sort_full_parallel((void*) &args1);
                quick_sort_full_parallel((void*) &args2);

        } else {
                quick_sort(b->start, b->end, b->dir);
        }

        return NULL;
}

/*
 *      An interative version of quick sort, which should be fast since
 *      recursion is slow.
 */

int partition_i (void* args)
{
    sort_args_t *b = (sort_args_t*) args;

    int x = global_array->data[b->end];
    int i = (b->start - 1);
    int j;

    for (j = b->start; j <= b->end - 1; j++)
    {
        if (global_array->data[j] <= x)
        {
            i++;
            swap (i, j);
        }
    }

    swap (i + 1, b->end);

    return (i + 1);
}

void* quick_sort_iterative(void* args)
{
    sort_args_t *b = (sort_args_t*) args;

    int *stack = (int*) malloc(2 * (b->end - b->start + 2) * sizeof(int));
    int count = -1;

    stack[++count] = b->start;
    stack[++count] = b->end;

    // until stack is empty
    while (count >= 0)
    {
        b->end = stack[count--];
        b->start = stack[count--];
        int p = partition_i(b);

        // if element left of pivot push left side to stack
        if (p - 1 > b->start)
        {
            stack[++count] = b->start;
            stack[++count] = p - 1;
        }

        // if element right of pivot push right side to stack
        if (p + 1 < b->end)
        {
            stack[++count] = p + 1;
            stack[++count] = b->end;
        }

    }
    return NULL;
}

/*
 *      A compare function for the in-build gcc quick sort. We
 *      don't use it, but it's interessting to see how fast it
 *      is. And it's f***ing fast...
 */
int cmp (const void* a, const void* b) {

        return *(int*)a < *(int*)b;
}

/*
        Sorts the array
*/
int sort(struct array * array)
{
        global_array = array;

        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

        pthread_mutex_init(&level_mutex, NULL);

#if OWN == 1

        #if DEBUG == 1
        printf("full parallel quicksort\n");
        #endif
        //~ pthread_t qsthreads[2];

        sort_args_t paras1 = { 0, global_array->length, true, 0, 0};
        //~ sort_args_t paras2 = { global_array->length/2, global_array->length, true, 0, 1};

        // uses outplace_array
        outplace_array = (value*) malloc(global_array->length * sizeof(value));

        quick_sort_full_parallel((void*) &paras1);
        //~ pthread_create(&qsthreads[0], &attr, quick_sort_full_parallel, (void*) &paras1);
        //~ pthread_create(&qsthreads[1], &attr, quick_sort_full_parallel, (void*) &paras2);
//~
        //~ pthread_join(qsthreads[0], NULL);
        //~ pthread_join(qsthreads[1], NULL);

        free(outplace_array);
        outplace_array = NULL;

        //

#elif OWN == 2

        #if DEBUG == 1
        printf("parallel bitonic sort\n");
        #endif

        sort_args_t paras = { 0, global_array->length, true };

        bitonic_sort_parallel((void*) &paras);

#elif OWN == 3

        #if DEBUG == 1
        printf("sequential bitonic sort\n");
        #endif

        bitonic_sort(0, global_array->length, true);

#elif OWN == 4

        #if DEBUG == 1
        printf("parallel quicksort\n");
        #endif

        sort_args_t paras = { 0, global_array->length, true };

        quick_sort_parallel((void*) &paras);

#elif OWN == 5

        #if DEBUG == 1
        printf("sequential quicksort\n");
        #endif

        quick_sort(0, global_array->length, true);

#elif OWN == 6

        #if DEBUG == 1
        printf("sequential selection sort\n");
        #endif

        selection_sort(0, global_array->length);

#elif OWN == 7

        #if DEBUG == 1
        printf("sequential insertion sort\n");
        #endif

        insertion_sort(0, global_array->length);

#elif OWN == 8

        #if DEBUG == 1
        printf("parallel quicksort\n");
        #endif

        sort_args_t paras = { 0, global_array->length, true };

        quick_sort_parallel_insertion((void*) &paras);

#elif OWN == 9

        #if DEBUG == 1
        printf("parallel bitonic quicksort sort\n");
        #endif

        sort_args_t paras = { 0, global_array->length, true };

        bitonic_quick_sort_parallel((void*) &paras);

#elif OWN == 10

        printf("iterative quick sort\n");
        // uses outplace_array
        outplace_array = (value*) malloc(global_array->length * sizeof(value));

        sort_args_t paras = { 0, global_array->length - 1, true };

        quick_sort_iterative((void*) &paras);

        free(outplace_array);

#elif OWN == 11

        #if NB_THREADS > 1
        int i, chunk=global_array->length/NB_THREADS, starts[NB_THREADS], ends[NB_THREADS];
        sort_args_t params[NB_THREADS];
        value *arr = global_array->data;

        outplace_array = (value*) malloc(global_array->length * sizeof(value));
        // sort just parts of array
        for(i=0; i<NB_THREADS; i++) {
                params[i].dir = true;
                params[i].start = i*chunk;
                params[i].end = (i+1)*chunk - 1;
                if ( i == NB_THREADS-1 ) params[i].end = global_array->length - 1;

                starts[i] = params[i].start;
                ends[i] = params[i].end;

                pthread_create(threads+i, &attr, quick_sort_iterative, (void*) (&params[i]));
        }
        array_printf(global_array);
        for(i=0; i<NB_THREADS; i++) pthread_join(threads[i], NULL);

        int cont = 1,out = 0, min = 0;
        // merge (outplace) these chunks of array
        while(cont != 0) {
                cont = NB_THREADS;
                for(i=0; i<NB_THREADS; i++) {
                        if (starts[i] > ends[i]) {
                                cont--;
                                continue;
                        }
                        min = ( arr[starts[min]] <= arr[starts[i]] ? min : i );
                }
                outplace_array[out] = arr[starts[min]];
                out++; starts[min]++;
        }
        for(i=0; i<NB_THREADS; i++) printf("%d,", starts[i]);
        for(i=0; i<NB_THREADS; i++) if (starts[i] == ends[i]) outplace_array[out] = arr[starts[i++]];

        free(global_array->data);
        global_array->data = outplace_array;
        array_printf(global_array);
        #else
                sort_args_t paras = { 0, global_array->length - 1, true };
                quick_sort_iterative((void*) &paras);
        #endif

#else

        #if DEBUG == 1
        printf("given sequential quicksort sort\n");
        #endif

        simple_quicksort_ascending(array);

#endif

        return 0;
}
