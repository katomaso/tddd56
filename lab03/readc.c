#include <stdio.h>

int main(int argc, char *argv[]) {
    int bytes, i;
    char c;
    FILE *fp;

    if(argc != 3) {
	fprintf(stdout, "Usage: $ readc <bytes> FILE\n");
	return 1;
    }

    bytes = atoi(argv[1]);
    if (bytes < 1 ) {
	fprintf(stderr, "<bytes> has to be greater then 0\n");
	return 1;
    }

    fp = fopen(argv[2], "rt");
    if (!fp) {
	fprintf(stderr, "File %s does not exist\n");
    }

    for(i=0; i<bytes; i++) {
	if((c = getc(fp)) == EOF) break;
	fprintf(stdout, "%c", c);
    }

    fprintf(stdout, "\n");
    fclose(fp);
    return 0;
}
