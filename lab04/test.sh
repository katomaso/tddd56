#!/bin/bash
rm rescpu.txt resgpu.txt
touch rescpu.txt
touch resgpu.txt

for I in `seq 8 16 2048`
do
	./cpu $I >> rescpu.txt
done

for I in `seq 8 16 2048`
do
	./gpu $I 32 >> resgpu.txt
done
