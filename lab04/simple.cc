// Simple example
// Assigns every element in an array with its index.

#include <stdio.h>
#include <math.h>

const int N = 16;

void simple(float *c)
{
    for(int i=0; i<N; i++)
        c[i] = sqrt(c[i]);
}

int main()
{
	float *c = new float[N];
	const int size = N*sizeof(float);
	for (int i = 0; i < N; i++) c[i] = (float) i*i+i+i;
	simple(c);
	for (int i = 0; i < N; i++)	printf("%f ", c[i]);
	printf("\n");

	delete[] c;
	printf("done\n");
	return 0;
}
