// Matrix addition, CPU version
// gcc matrix_cpu.c -o matrix_cpu -std=c99

#include <stdio.h>

// one dimension of matrix
#define NN 1024

// block and grid are 1D
#define THREADS 16  // max 512
#define BLOCKS 64 // max 65 535

#ifndef DEBUG
#define DEBUG 0
#endif

__global__
void add_matrix(float *a, float *b, float *c, int N)
{
	// too big overkill
	//int idx = (gridDim.x * blockDim.x * blockIdx.y) + \
		(gridDim.x * blockDim.x * threadIdx.y) + \
		(blockDim.x * blockIdx.x) + \
		threadIdx.x;
		
		
	int idx = (gridDim.y * blockDim.y * blockIdx.x) + \
		(gridDim.y * blockDim.y * threadIdx.x) + \
		(blockDim.y * blockIdx.y) + \
		threadIdx.y;
	// Look'ma!
	//int idx = (blockDim.x * blockIdx.x) + threadIdx.x;
	if(idx < N) {
		c[idx] = a[idx] + b[idx];
	}
}

int main(int argc, char* argv[])
{
	int N, nthreads;

	if(argc >= 2) N = atoi(argv[1]);
	else N = NN;
	
	if(argc >= 2) nthreads = atoi(argv[2]);
	else nthreads = THREADS;

	if(argc != 2) fprintf(stderr, "proper usage: gpu N\n");

	const int MS = sizeof(float) * N * N; // stands for matrix size

	float *a = new float[N*N];
	float *b = new float[N*N];
	float *c = new float[N*N];

	float *d_a, *d_b, *d_c;
	float milis;

	cudaEvent_t start, stop;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{
			a[i+j*N] = 10 + i;
			b[i+j*N] = (float)j / N;
		}

#if DEBUG == 1
	if(N < 128)
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			printf("%0.2f ", a[i+j*N]);
		}
		printf("\n");
	}
#endif

#if DEBUG == 1
	if(N < 128)
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			printf("%0.2f ", b[i+j*N]);
		}
		printf("\n");
	}
#endif
	cudaMalloc((void**)&d_a, MS);
	cudaMalloc((void**)&d_b, MS);
	cudaMalloc((void**)&d_c, MS);

	cudaMemcpy(d_a, a, MS , cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, MS , cudaMemcpyHostToDevice);
	cudaThreadSynchronize();
	cudaEventRecord(start, 0);
	cudaEventSynchronize(start);

	dim3 blocks( (N+nthreads-1)/nthreads, (N+nthreads-1)/nthreads );
	dim3 threads(nthreads, nthreads);

	add_matrix<<<blocks, threads>>>(d_a, d_b, d_c, N*N);
	cudaThreadSynchronize();

	// measure end of the program
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milis, start, stop);
	cudaMemcpy(c, d_c, MS, cudaMemcpyDeviceToHost);

	cudaEventDestroy(start);
	cudaEventDestroy(stop);

#if DEBUG == 1
	if(N < 128)
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			printf("%0.2f ", c[i+j*N]);
		}
		printf("\n");
	}
#endif

	printf("%d\t%f\n", N, milis);

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);

	delete [] a;
	delete [] b;
	delete [] c;

}
