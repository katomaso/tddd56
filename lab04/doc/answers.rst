Laboration 4: Introduction to CUDA
==================================

Part 1. Trying out CUDA
------------------------
**a) Compile and run simple.cu**

a) *How many cores will simple.cu use, max, as written?*

 We use grid of size (1,1) and blocks with 16 thread in row. That means we use 1 multicore with 16 threads (cores)
 which means 16 parallel processes.

**b) Modifying simple.cu**

b) *Is the calculated square root identical to what the CPU calculates? Should we assume that this is always the case?*

  No, they aren't.
  
  GPU output: ``0.000000 1.732051 2.828427 3.872983 4.898980 5.916080 6.928204 7.937254 8.944273 9.949875 10.954452 11.958261 12.961482 13.964240 14.966630 15.968719``
  
  CPU output: ``0.000000 1.732051 2.828427 3.872983 4.898980 5.916080 6.928203 7.937254 8.944272 9.949874 10.954452 11.958261 12.961481 13.964240 14.966630 15.968719``

  We have two ideas what can be the reason for different results. First is that GPU and CPU has different
  size for floating points. The other is that they use different representation of floating point numbers.

Part 2. Performance and block size
----------------------------------
**a) Array computation from C to CUDA**

a) *How do you calculate the index in the array, using 2-dimensional blocks?*

  In following code we assume 2-dimensional grid (just to make it little bit
  harder)
  
  ``int index = (gridDim.x * blockDim.x * blockIdx.y + blockDim.x * blockIdx.x )``
  ``+ (blockDim.x * threadIdx.y + threadIdx.x);``


**b) Larger data set and timing with CUDA Events**

a) *What happens if you use too many threads per block? (Hint: You can get misleading output if you don't clear your buffers.)*

  We don't know.

b) *At what data size is the GPU faster than the CPU?*

  Our measurement shows that GPU outperformed CPU when N is greater then 400.
  (When using static number of threads - 128)

c) *What block size seems like a good choice? Compared to what?*

  It seems that the the best is to use 10000 blocks and the rest try to fill
  with threads. 

d) *Write down your data size, block size and timing data for the best GPU performance you can get.*

  N=(750*750)=562500, #blocks=5625, #threads=100

  .. image:: ./performance.png
     :width: 250px
     :height: 100px



**c) Coalescing**

a) *How much performance did you lose by making data accesses non-coalesced?*

  For testing we used matrix 4096² with 32² threads and 128² blocks.
  Coealesced access: 1.898688 miliseconds
  Non-coealesced access: 14.427072 miliseconds


3. One more time on basic performance - Interactive Julia
---------------------------------------------------------
a) *What was the problem?*
b) *How did you correct it?*

  The vanilla program used only one thread per block. The initial image then took
  about 131.5 miliseconds.
  When we are using 32 threads per block then we get 1.65 miliseconds per initial
  image.
  
c) *What speedup did you get?*

  The speedup is then about 100times.
  


4. Beefing up the interactive fractal (non-mandatory, if you have time)
-----------------------------------------------------------------------
a) *What kind of modifications did you implement?*

