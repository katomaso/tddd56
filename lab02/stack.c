/*
 * stack.c
 *
 *  Created on: 18 Oct 2011
 *  Copyright 2011 Nicolas Melot
 *
 * This file is part of TDDD56.
 *
 *     TDDD56 is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     TDDD56 is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with TDDD56. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DEBUG
#define NDEBUG
#endif

#include <assert.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "stack.h"
#include "non_blocking.h"

#if NON_BLOCKING == 0
#warning Stacks are synchronized through locks
#elif NON_BLOCKING == 1
#warning Stacks are synchronized through lock-based CAS
#else
#warning Stacks are synchronized through hardware CAS
#endif

int PTR_SIZE = sizeof(void*)*8;

void print(stack_t* stack) {

    item_t *item = stack->head;

    printf("[");

    while(item) {
        printf("%d, ", *(int*)item->value);
        item = item->next;
    }

    printf("]\n");
}

stack_t * stack_alloc()
{
    // Example of a task allocation with correctness control
    // Feel free to change it
    stack_t *res;

    res = malloc(sizeof(stack_t));
    assert(res != NULL);

    if (res == NULL)
        return NULL;

// You may allocate a lock-based or CAS based stack in
// different manners if you need so
#if NON_BLOCKING == 0

    // Implement a lock_based stack

#elif NON_BLOCKING == 1

    // Implement a software-based CAS stack

#else
    // Implement a hardware-based CAS stack
#endif

    return res;
}

int stack_init(stack_t *stack, size_t size)
{
    assert(stack != NULL);

    stack->head = NULL;
    stack->data_size = size;

#if NON_BLOCKING == 0

    // Implement a lock_based stack
    pthread_mutex_init(&stack->lock, NULL);

#elif NON_BLOCKING == 1
    // Implement a software-based CAS stack
    pthread_mutex_init(&stack->lock, NULL);

#else
    // Implement a hardware-based CAS stack
#endif

    return 0;
}

int stack_push_safe(stack_t *stack, void* buffer)
{
#if NON_BLOCKING == 1 || NON_BLOCKING == 2
    void *sanitized;
#endif
    if(buffer == NULL) return 0;
    item_t *item = (item_t*) buffer;

#if NON_BLOCKING == 0

    pthread_mutex_lock(&stack->lock);

      item->next = stack->head;
      stack->head = item;

    pthread_mutex_unlock(&stack->lock);

#elif NON_BLOCKING == 1
    // Implement a software-based CAS stack
    item->next = stack->head;

    while( software_cas( (size_t*)&stack->head, (size_t)item->next, (size_t)item, &stack->lock) != (size_t)item->next)
    {
       item->next = stack->head;
    }

#else
    // Implement a software-based CAS stack
    item->next = stack->head;

    while( cas( (size_t*)&stack->head, (size_t)item->next, (size_t)item ) != (size_t)item->next)
    {
      item->next = stack->head;
    }

#endif

    return 1;
}

int stack_pop_safe(stack_t *stack, void* buffer)
{
    item_t *item;

    assert(buffer != NULL);
    if (stack->head == NULL) {
        printf("no items on stack (not an error)\n");
        return 0;
    }

#if NON_BLOCKING == 0
#warning lock pop
    // Implement a lock_based stack
    pthread_mutex_lock(&stack->lock);

      item = stack->head;
      stack->head = item->next;

    pthread_mutex_unlock(&stack->lock);

#elif NON_BLOCKING == 1
#warning soft pop
    // Implement a software-based CAS stack
    item = stack->head;

    while(software_cas( (size_t*)&stack->head, (size_t)item, (size_t)item->next, &stack->lock) != (size_t)item)
    {
      item = stack->head;
    }

#else
#warning hard pop
    // Implement a software-based CAS stack
    item = stack->head;

    while(cas( (size_t*)&stack->head, (size_t)item, (size_t)item->next) != (size_t)item)
    {
      item = stack->head;
    }

#endif

    memcpy(buffer, item, sizeof(item_t));
    return 1;
}


int stack_check(stack_t *stack)
{
    assert(stack != NULL);

    return 0;
}


void stack_destroy(stack_t* stack) {
  /*
  void* sanitized = (void*) stack->head;
  item_t *item = aba_unsanitize(sanitized);

  while(item) {
    sanitized = (void*) item->next;
    free(item->value);
    free(item);
    item = aba_unsanitize(sanitized);
  }
  */
  #if NON_BLOCKING == 0 || NON_BLOCKING == 1
  pthread_mutex_destroy( &stack->lock );
  # endif

  free(stack);
}

int stack_size(stack_t* stack) {

  item_t* item = stack->head;
  int size = 0;

  while(item) {
    item = item->next;
    size++;
  }
  return size;
}


void* aba_sanitize(item_t* ptr) {
/*  
static int counter = 0;
  counter = ((counter + 1) % 8);

  #if 0 && __x86_64__ || __LP64__ 
  #warning Arch64
  return (void*)((size_t)ptr | (counter << (PTR_SIZE-3)));
  #else
*/
  return (void*) ptr;
  //#endif
}

item_t* aba_unsanitize(void* ptr) {
/*
  #if 0 && __x86_64__ || __LP64__ 
  return (item_t*)((size_t)ptr << 3 >> 3);
  #else
*/
  return (item_t*) ptr;
  //#endif
}





/* ******************* ABA simulation HERE ******************************* */
#ifdef STANDALONE

stack_t *stack;

pthread_mutex_t abalock;

pthread_cond_t cond;



int stack_push1_safe(stack_t *stack, void* buffer)
{

    item_t *item = (item_t*) buffer;
    if(buffer == NULL) return 0;
    
    // Implement a software-based CAS stack
    item->next = stack->head;
    size_t next = (size_t)stack->head->next;
    printf("T1: ABA push - got head address %lx and next is %lx and going to sleep\n", (size_t)stack->head, (size_t)stack->head->next);

    pthread_cond_signal(&cond);
    pthread_cond_wait(&cond, &abalock);

    printf("T1: waking up, address of the head is %lx and value is %lx\n", (size_t)stack->head, (size_t)stack->head->next);
    
	if(stack->head == item->next && next != stack->head->next) 
		printf("Head is the same but NEXT is NOT. ABA!!!!!\n");

    while( cas( (size_t*)&stack->head, (size_t)item->next, (size_t)item) != (size_t)item->next)
    {
       item->next = stack->head;
	printf("T1: CAS failed, going next round\n");
    }
    
    printf("T1: CAS succeded \n");
    
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&abalock);
    return 1;
}



void* thread1(void* args) {    
    int a = 1, b = 2, c = 3, tmp;
    item_t *it = malloc(sizeof(item_t)*4);
    printf("T1: push three times\n");
    stack_push_safe(stack, &it[0]);
    stack_push_safe(stack, &it[1]);
    stack_push_safe(stack, &it[2]);

    stack_push1_safe(stack, &it[3]);
    
   return NULL;
}

void* thread2(void* args) {
   item_t *it = malloc(sizeof(item_t));
    
	pthread_mutex_lock(&abalock);
	pthread_cond_wait(&cond, &abalock);

        printf("T2: waking up, doing ABA\n");

	item_t *prevhead = stack->head;
	item_t itmp;
	
	stack_pop_safe(stack, &itmp);
	stack_pop_safe(stack, &itmp);
	
	// we can change the prevhead

	stack_push_safe(stack, prevhead);	

	
	printf("T2: 2*poped and 2*pushed, going to sleep\n");

	pthread_cond_signal(&cond);
	pthread_mutex_unlock(&abalock);

	return NULL;

}


int main(void)
{

    int i;
    pthread_attr_t attr;
    pthread_t threads[2];

    stack = stack_alloc();

    stack_init(stack, sizeof(int));

    pthread_attr_init(&attr);

    pthread_mutex_init(&abalock, NULL);

    pthread_cond_init(&cond, NULL);
    

    pthread_create(&threads[0], &attr, &thread1, NULL);
    pthread_create(&threads[1], &attr, &thread2, NULL);


    for (i = 0; i < 2; i++)
    {
      pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&abalock);

    pthread_cond_destroy(&cond);
    pthread_attr_destroy(&attr);

    stack_destroy(stack);

    return 0;
}
#endif
