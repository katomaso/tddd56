/*
 * stack.h
 *
 *  Created on: 18 Oct 2011
 *  Copyright 2011 Nicolas Melot
 *
 * This file is part of TDDD56.
 *
 *     TDDD56 is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     TDDD56 is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with TDDD56. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <pthread.h>

#ifndef STACK_H
#define STACK_H

#define STACK_SIZE 1000


struct item {
  struct item* next;
  void* value;
};
typedef struct item item_t;

struct stack
{
    item_t* head;
    size_t data_size;
    pthread_mutex_t lock;
};

typedef struct stack stack_t;

void print(stack_t*);

// Allocates and return a stack data structure
stack_t *stack_alloc();

// Initialize a newly allocated stack
int stack_init(stack_t *, size_t);

// Pushes an element in a thread-safe manner
int stack_push_safe(stack_t *, void*);

// Pops an element in a thread-safe manner
int stack_pop_safe(stack_t *, void*);

// Check the functional state of a stack
int stack_check(stack_t *);

void stack_destroy(stack_t*);

int stack_size(stack_t*);

void* aba_sanitize(item_t*);
item_t* aba_unsanitize(void*);

#endif /* STACK_H */
