:Author: Tomas Peterka & Thomas Uhrig

Lab 5: Memory addressing, synchronization and shared memory in CUDA
===================================================================


**QUESTION:** *How much data did you put in shared memory?*

  We use size of shared memory directly dependent on number of threads. Testing
  showed that reasonable chunks for image 512x512 are **8x8, 16x16 and 32x32**.

**QUESTION:** *How much data does each thread copy to shared memory?*

  Each non-border thread copies one pixel into shared memory. Border pixels
  copies two neighbour as well and corner threads has to copy 4 additional
  pixels.

**QUESTION:** *How did you handle the necessary overlap between the blocks?*

  The overlap is handled by making shared memory 2 pixels wider on each side.
  The widened borders are filled with appropriate data by border threads thus
  every thread has sufficient amount of source data.

**QUESTION:** *If we would like to increase the block size, about how big blocks would be safe to use in this case? Why?*

  The maximum size of shared block is limited by two things
   * the hardware limit of threads per block
   * the hardware limit on size of shared memory

Speedup obtained with the best-performing blocksize 16 is up to **11.8x**
