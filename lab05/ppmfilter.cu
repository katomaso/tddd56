#include <stdio.h>
#include "readppm.c"

#ifdef __APPLE__
	#include <GLUT/glut.h>
	#include <OpenGL/gl.h>
#else
	#include <GL/glut.h>
#endif

#ifndef SHMW
#define SHMW 16
#endif


#define SHMROW(R) ((SHMW+4)*3*(R))
#define PIXEL_COPY(shmIndex, imageIndex) shMem[shmIndex+0] = image[imageIndex+0];\
		shMem[shmIndex+1] = image[imageIndex+1];\
		shMem[shmIndex+2] = image[imageIndex+2];\

__global__ void filter(unsigned char *image, unsigned char *out, int n, int m)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	int sumx, sumy, sumz, k, l;

	// set upt shared memory

	unsigned int Idx = y*n*3 + x*3;

	//if(i<2 || i>=n-2 || y<2 || y>=m-2)
	//	return;

	__shared__ unsigned char shMem[(3*(SHMW+4) * (SHMW+4))];

	unsigned int shY = threadIdx.y + 2;
	unsigned int shX = threadIdx.x + 2;

	/*Only The boundary threads of Thread-Block will do extra effort of padding*/
	if (threadIdx.x==0)
	{
		// pad
		PIXEL_COPY(SHMROW(shY)+shX*3-3, Idx-3)
		PIXEL_COPY(SHMROW(shY)+shX*3-6,Idx-6);

	}
	if(threadIdx.x==SHMW-1)
	{
		// pad
		PIXEL_COPY(SHMROW(shY)+(shX+1)*3, Idx+3);

		shMem[SHMROW(shY)+shX*3+0+6] = image[Idx+6+0];
		shMem[SHMROW(shY)+shX*3+1+6] = image[Idx+6+1];
		shMem[SHMROW(shY)+shX*3+2+6] = image[Idx+6+2];
	}
	if(threadIdx.y==0 && blockIdx.y > 0)
	{
		// pad
		shMem[SHMROW(shY-1)+shX*3+0] = image[Idx-n*3+0];
		shMem[SHMROW(shY-1)+shX*3+1] = image[Idx-n*3+1];
		shMem[SHMROW(shY-1)+shX*3+2] = image[Idx-n*3+2];

		shMem[SHMROW(shY-2)+shX*3+0] = image[Idx-6*n+0];
		shMem[SHMROW(shY-2)+shX*3+1] = image[Idx-6*n+1];
		shMem[SHMROW(shY-2)+shX*3+2] = image[Idx-6*n+2];
	}
	if(threadIdx.y==SHMW-1)
	{
		// pad
		shMem[SHMROW(shY+1)+shX*3+0] = image[Idx+n*3+0];
		shMem[SHMROW(shY+1)+shX*3+1] = image[Idx+n*3+1];
		shMem[SHMROW(shY+1)+shX*3+2] = image[Idx+n*3+2];

		shMem[SHMROW(shY+2)+shX*3+0] = image[Idx+n*6+0];
		shMem[SHMROW(shY+2)+shX*3+1] = image[Idx+n*6+1];
		shMem[SHMROW(shY+2)+shX*3+2] = image[Idx+n*6+2];
	}
	// left upper corner
	if(threadIdx.y==0 && threadIdx.x==0 && blockIdx.y > 0 && blockIdx.x > 0) {
		PIXEL_COPY(SHMROW(shY-1)+(shX-1)*3, Idx-3 - (3*n)); // x-1, y-1
		PIXEL_COPY(SHMROW(shY-1)+(shX-2)*3, Idx-6 - (3*n)); // x-2, y-1
		PIXEL_COPY(SHMROW(shY-2)+(shX-1)*3, Idx-3 - (6*n)); // x-1, y-2
		PIXEL_COPY(SHMROW(shY-2)+(shX-2)*3, Idx-6 - (6*n)); // x-2, y-2
	}
	// right upper corner
	if(threadIdx.y==0 && threadIdx.x==SHMW-1 && blockIdx.y > 0 && blockIdx.x > 0) {
		PIXEL_COPY(SHMROW(shY-1)+(shX+1)*3, Idx+3 - (3*n)); // x+1, y-1
		PIXEL_COPY(SHMROW(shY-1)+(shX+2)*3, Idx+6 - (3*n)); // x+2, y-1
		PIXEL_COPY(SHMROW(shY-2)+(shX+1)*3, Idx+3 - (6*n)); // x+1, y-2
		PIXEL_COPY(SHMROW(shY-2)+(shX+2)*3, Idx+6 - (6*n)); // x+2, y-2
	}
	// left bottom
	if(threadIdx.y==SHMW-1 && threadIdx.x==0) {
		PIXEL_COPY(SHMROW(shY+1)+(shX-1)*3, Idx-3 + (3*n)); // x-1, y+1
		PIXEL_COPY(SHMROW(shY+1)+(shX-2)*3, Idx-6 + (3*n)); // x-2, y+1
		PIXEL_COPY(SHMROW(shY+2)+(shX-1)*3, Idx-3 + (6*n)); // x-1, y+2
		PIXEL_COPY(SHMROW(shY+2)+(shX-2)*3, Idx-6 + (6*n)); // x-2, y+2
	}
	// right bottom
	if(threadIdx.y==SHMW-1 && threadIdx.x==SHMW-1) {
		PIXEL_COPY(SHMROW(shY+1)+(shX+1)*3, Idx+3 + (3*n)); // x+1, y+1
		PIXEL_COPY(SHMROW(shY+1)+(shX+2)*3, Idx+6 + (3*n)); // x+2, y+1
		PIXEL_COPY(SHMROW(shY+2)+(shX+1)*3, Idx+3 + (6*n)); // x+1, y+2
		PIXEL_COPY(SHMROW(shY+2)+(shX+2)*3, Idx+6 + (6*n)); // x+2, y+2
	}


	shMem[SHMROW(shY)+shX*3+0] = image[Idx+0];
	shMem[SHMROW(shY)+shX*3+1] = image[Idx+1];
	shMem[SHMROW(shY)+shX*3+2] = image[Idx+2];

	__syncthreads();

	// filtering something


	// printf is OK under --device-emulation
	//	printf("%d %d %d %d\n", i, y, n, m);


	if (y < m && x < n)
	{
		//out[(i*n+y)*3+0] = shMem[(i*n+y)*3+0];
		//out[(i*n+y)*3+1] = shMem[(i*n+y)*3+1];
		//out[(i*n+y)*3+2] = shMem[(i*n+y)*3+2];

		out[(x+n*y)*3+0] = shMem[(shY*(SHMW+4)+shX)*3+0];
		out[(x+n*y)*3+1] = shMem[(shY*(SHMW+4)+shX)*3+1];
		out[(x+n*y)*3+2] = shMem[(shY*(SHMW+4)+shX)*3+2];
	}


	if (x > 1 && x < m-2 && y > 1 && y < n-2)
	{
		// Filter kernel
		sumx=0;sumy=0;sumz=0;

		for(k=-2;k<3;k++) {

			for(l=-2;l<3;l++)
			{
				sumx += shMem[((shX+k)+(SHMW+4)*(shY+l))*3+0];
				sumy += shMem[((shX+k)+(SHMW+4)*(shY+l))*3+1];
				sumz += shMem[((shX+k)+(SHMW+4)*(shY+l))*3+2];
			}
		}

		out[(x+n*y)*3+0] = sumx/25;
		out[(x+n*y)*3+1] = sumy/25;
		out[(x+n*y)*3+2] = sumz/25;
	}
}


// Compute CUDA kernel and display image
void Draw()
{
	unsigned char *image, *out;
	unsigned char *dev_image, *dev_out;
	int n, m;
	float milis;
	cudaEvent_t start, stop;

	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	image = readppm("maskros512.ppm", &n, &m);
	out = (unsigned char*) malloc(n*m*3);

	// allocate memory on the graphic card
	cudaMalloc( (void**)&dev_image, n*m*3);
	cudaMalloc( (void**)&dev_out, n*m*3);

	// copy to global memory on the graphic card
	cudaMemcpy( dev_image, image, n*m*3, cudaMemcpyHostToDevice);

	cudaThreadSynchronize();
	cudaEventRecord(start, 0);
	cudaEventSynchronize(start);

	// set up grids and blocks
	dim3 dimBlock( SHMW, SHMW );
	dim3 dimGrid( (n+SHMW-1)/SHMW, (m+SHMW-1)/SHMW );

	// call the kernel
	filter<<<dimGrid, dimBlock>>>(dev_image, dev_out, n, m);

	// wait until all finished on the graphic card
	cudaThreadSynchronize();
	// measure end of the program
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&milis, start, stop);

	// copy back from the graphic card
	cudaMemcpy( out, dev_out, n*m*3, cudaMemcpyDeviceToHost );

	// destroy event
	cudaEventDestroy(start);
	cudaEventDestroy(stop);

	// free the allocated memory on the graphic card
	cudaFree(dev_image);
	cudaFree(dev_out);

	fprintf(stdout, "%d\t%f\n", SHMW, milis);
	// Dump the whole picture onto the screen.
	glClearColor( 0.0, 0.0, 0.0, 1.0 );
	glClear( GL_COLOR_BUFFER_BIT );
	glRasterPos2f(-1, -1);
	glDrawPixels( n, m, GL_RGB, GL_UNSIGNED_BYTE, image );
	glRasterPos2i(0, -1);
	glDrawPixels( n, m, GL_RGB, GL_UNSIGNED_BYTE, out );
	glFlush();
}

// Main program, inits
int main( int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_SINGLE | GLUT_RGBA );
	glutInitWindowSize( 1024, 512 );
	glutCreateWindow("CUDA on live GL");
	glutDisplayFunc(Draw);

	glutMainLoop();
}
